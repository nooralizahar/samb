<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', [App\Http\Controllers\FormController::class, 'home'])->name('index');

Route::get('/form/{title?}', [App\Http\Controllers\FormController::class, 'index']);
Route::post('/form/save', [App\Http\Controllers\FormController::class, 'save']);
