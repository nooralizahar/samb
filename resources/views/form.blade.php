<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Laravel</title>
    
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/features/">
    
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta name="theme-color" content="#7952b3">
    
    
    <!-- Custom styles for this template -->
    <link href="feature.css" rel="stylesheet">
</head>
<body class="">
    
    <div class="container px-4 py-5" id="custom-cards">
        <h2 class="pb-2 border-bottom">Apply Job Vacancy For {{ $title ?? '' }}</h2>
        
        <div class="row g-4 py-5">
            <div class="col">
                <form action="/form/save" method="POST" id="form">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h4>Form</h4>
                        </div>
                        <div class="card-body">
                            <div class="mb-3">
                                <label for="" class="form-label">Full Name</label>
                                <input type="text" name="name" class="form-control" id="name" aria-describedby="" required>
                            </div>
                            <div class="mb-3">
                                <label for="" class="form-label">Phone Number</label>
                                <input type="number" class="form-control" id="phone" aria-describedby="" required>
                            </div>
                            <div class="mb-3">
                                <label for="" class="form-label">Whatsapp Number</label>
                                <input type="number" class="form-control" id="wa" aria-describedby="" required>
                            </div>
                            <div class="mb-3">
                                <label for="" class="form-label">Applied Position</label>
                                <input type="text" name="apply" class="form-control" id="position" value="{{ $title ?? '' }}" aria-describedby="" readonly required>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="/" class="btn btn-warning">Back</a>
                            <button type="submit" class="btn btn-primary confirm" style="float: right">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Creates the bootstrap modal where the image will appear -->
    <div class="modal fade imagemodal" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">       
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>       
                <div class="modal-body" style="text-align: center">
                    <img src="" class="imagepreview" style="width: 410px; height:610px" >
                </div>
            </div>
        </div>
    </div>
</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script>
    $('.confirm').on('click', function (e) {
        var name = $.trim($('#name').val());
        var phone = $.trim($('#phone').val());
        var wa = $.trim($('#wa').val());
        var position = $.trim($('#position').val());
        if (name  === '' || phone  === '' || wa  === '' || position  === '') {
            alert('Text-field is empty.');
            return false;
        }
        
        e.preventDefault();
        var data = $(this).serialize();
        swal({
            title: "Application Confirmation",
            text: "Are you sure you want to continue this application?",
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function (value) {
            if(value){
                $('#form').submit();
            }
        });
        return false;
    });
</script>
</html>

