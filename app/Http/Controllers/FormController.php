<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function home()
    {
        return view('index');
    }
    public function index($title = null)
    {
        $data['title'] = $title;
        return view('form', $data);
    }

    public function save(Request $request)
    {
        return redirect()->route('index')->with(['success' => 'Mr/Mrs. '.$request->name.' successfully applied as '.$request->apply]);
    }
}
