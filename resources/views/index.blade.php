<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Laravel</title>
    
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/features/">
    
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta name="theme-color" content="#7952b3">
    
    
    <!-- Custom styles for this template -->
    <link href="feature.css" rel="stylesheet">
</head>
<body class="">
    
    <div class="container px-4 py-3" id="custom-cards">
        <h2 class="pb-2 border-bottom">Job Vacancy</h2>
        
        @if (session('error'))
        <div class="alert alert-danger"><i class="flaticon-exclamation text-danger"></i> {{ session('error') }}</div>
        @elseif (session('success'))
        <div class="alert alert-success"><i class="flaticon-exclamation text-success"></i> {{ session('success') }}</div>
        @endif

        <div class="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5">
            <div class="col pop">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('/picture/police.jpg');">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h3 class="pt-5 mt-5 mb-4 lh-1 fw-bold">Looking For Police Position</h3>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="/form/Police"><button class="btn btn-success">Apply</button></a>
                            </li>
                            <li class="d-flex align-items-center me-3">
                                <small>Depok</small>
                            </li>
                            <li class="d-flex align-items-center">
                                <small>Rp. 5.000.000,-</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col pop">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('/picture/doctor.jpg');">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h3 class="pt-5 mt-5 mb-4 lh-1 fw-bold">Looking For Doctor Position</h3>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="/form/Doctor"><button class="btn btn-success">Apply</button></a>
                            </li>
                            <li class="d-flex align-items-center me-3">
                                <small>Padang</small>
                            </li>
                            <li class="d-flex align-items-center">
                                <small>Rp. 5.000.000,-</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col pop">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('/picture/firefighter.jpg');">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h3 class="pt-5 mt-5 mb-4 lh-1 fw-bold">Looking For Firefighter Position</h3>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="/form/Firefighter"><button class="btn btn-success">Apply</button></a>
                            </li>
                            <li class="d-flex align-items-center me-3">
                                <small>Medan</small>
                            </li>
                            <li class="d-flex align-items-center">
                                <small>Rp. 5.000.000,-</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="col pop">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('/picture/teacher.jpg');">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h3 class="pt-5 mt-5 mb-4 lh-1 fw-bold">Looking For Teacher Position</h3>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="/form/Teacher"><button class="btn btn-success">Apply</button></a>
                            </li>
                            <li class="d-flex align-items-center me-3">
                                <small>Bogor</small>
                            </li>
                            <li class="d-flex align-items-center">
                                <small>Rp. 5.000.000,-</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col pop">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('/picture/pilot.jpg');">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h3 class="pt-5 mt-5 mb-4 lh-1 fw-bold">Looking For Pilot Position</h3>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="/form/Pilot"><button class="btn btn-success">Apply</button></a>
                            </li>
                            <li class="d-flex align-items-center me-3">
                                <small>Malang</small>
                            </li>
                            <li class="d-flex align-items-center">
                                <small>Rp. 5.000.000,-</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col pop">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('/picture/programmer.jpg');">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h3 class="pt-5 mt-5 mb-4 lh-1 fw-bold">Looking For Programmer Position</h3>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="/form/Programmer"><button class="btn btn-success">Apply</button></a>
                            </li>
                            <li class="d-flex align-items-center me-3">
                                <small>Surabaya</small>
                            </li>
                            <li class="d-flex align-items-center">
                                <small>Rp. 5.000.000,-</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="col pop">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('/picture/craftmans.jpg');">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h3 class="pt-5 mt-5 mb-4 lh-1 fw-bold">Looking For Craftmans Position</h3>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="/form/Craftmans"><button class="btn btn-success">Apply</button></a>
                            </li>
                            <li class="d-flex align-items-center me-3">
                                <small>Tangerang</small>
                            </li>
                            <li class="d-flex align-items-center">
                                <small>Rp. 5.000.000,-</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col pop">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('/picture/chef.jpg');">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h3 class="pt-5 mt-5 mb-4 lh-1 fw-bold">Looking For Chef Position</h3>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="/form/Chef"><button class="btn btn-success">Apply</button></a>
                            </li>
                            <li class="d-flex align-items-center me-3">
                                <small>Bekasi</small>
                            </li>
                            <li class="d-flex align-items-center">
                                <small>Rp. 5.000.000,-</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col pop">
                <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" style="background-image: url('/picture/architect.jpg');">
                    <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                        <h3 class="pt-5 mt-5 mb-4 lh-1 fw-bold">Looking For Architect Position</h3>
                        <ul class="d-flex list-unstyled mt-auto">
                            <li class="me-auto">
                                <a href="/form/Architect"><button class="btn btn-success">Apply</button></a>
                            </li>
                            <li class="d-flex align-items-center me-3">
                                <small>Jakarta</small>
                            </li>
                            <li class="d-flex align-items-center">
                                <small>Rp. 5.000.000,-</small>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Creates the bootstrap modal where the image will appear -->
    <div class="modal fade imagemodal" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">       
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>       
                <div class="modal-body" style="text-align: center">
                    <img src="" class="imagepreview" style="width: 410px; height:610px" >
                </div>
            </div>
        </div>
    </div>
</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script>
    $(function() {
        $('.pop').on('click', function(e) {
            var bg = $(this).find('.card').css("background-image")
            src = bg.replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '')
            $('.imagepreview').attr('src', src);
            if($(e.target).closest('a').length){
                $('.imagemodal').modal('hide');
            } else {
                $('.imagemodal').modal('show');  
            }
        });		
    });
</script>
</html>

